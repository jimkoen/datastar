# Datastar

An educational text-based space sim written and played in SQL!

## Installation

To install the game, simply clone the repo

```
git clone https://gitlab.com/leifhka/datastar
```

Then execute the `datastar.sql` in a database:

```
cd datastar/src/
psql <flags> -f datastar.sql
```

where `<flags>` are the connection flags (database name, username, host, etc.).
This will at the end of the script prompt you for a selection of faction to play as,
with a short description of the faction. Note that the factions have slightly different
starting statistics, according to the description (e.g. `sznerckk` is more efficient miners
than the other factions).

After the setup, the game is played by logging into the database, e.g. with `psql`
or [Edbit](https://gitlab.com/leifhka/edbit). Edbit has some features which are very helpful
when playing, such as monitored windows (more on this below).

If you want to start a new game, execute the `clean.sql`-script to delete all data,
and then re-execute the `datastar.sql`-script, i.e. from `datastar/src`:

```
psql <flags> -f clean.sql
psql <flags> -f datastar.sql
```

### Updating to new Datastar version

Datastar is in constant development, so new updates come every now and then.
It is currently not possible to update to a new version and keep your current game,
thus the following update pricedure will clean the database and set up a new
game with the newest version.

To update the game, execute these commands from the `datastar`-folder in order:

```
psql <flags> -f src/clean.sql # Execute old clean-script
git pull origin main          # Update game-files
cd src
psql <flags> -f datastar.sql  # Create new game with updated game
```

## Playing the game

The player can use the information in the tables described in
```sql
SELECT * FROM game_tables;
```
and act using the functions described in
```sql
SELECT * FROM game_functions;
```

The game is turn-based/tick-based, and each turn/tick the player can
move once (via `fly` or `warp`) and do one action (`mine`, `buy`, `sell`) in
any order. Execute `tick()` to advance to the next turn/tick.

To execute a function, simply wrap it in a `SELECT`-clause. E.g.~to first fly to
planet `3`, then `mine`, then advance to next tick, simply execute the commands:

```sql
SELECT fly(3);
SELECT mine();
SELECT tick();
```

These can, of course, also be put into the same query:

```sql
SELECT fly(3), mine(), tick();
```

The goal of the game is to maximize the `money_rating` (as can be seen in the `status`-table), which
is a measure of how much money you have compared to all other actors in the universe. The universe
contains many other actors that are also acting every turn, these needs to be taken into account
when planning a stategy. To gain money, one can e.g.~buy a resource cheap and sell for a higher price
to a different planet (needing the resource more), or mine resources and sell to a needing planet.

## Tick-commands

At any point in time, the player can only get information local to the player. E.g.~the player
can only view the market at the player's current planet. You can of course store historical information
in your own tables, e.g.:

```sql
CREATE TABLE historical_prices AS
SELECT *
FROM market;
```
and then, on each tick execute
```sql
INSERT INTO historical_prices
SELECT * FROM market;
```

However, there might be alot of historical information one wants to
store. Therefore, the game has a feature called tick-commands. A tick-command
is a normal player-defined SQL-command that is executed
at every tick, that can be any `INSERT`, `UPDATE`, `DELETE`, etc. command.
Tick-commands are created and deleted with the functions `create_tick_command` and `delete_tick_command`.
So to store all markets seens, execute:

```sql
CREATE TABLE historical_markets(
  tick int, planet text, system text, -- context
  id int, resource text, sell_price float, buy_price float -- market
);
SELECT insert_tick_command(
    'markets',
    'INSERT INTO historical_markets
     SELECT t.tick, s.cur_planet, s.cur_system,
            r.id, r.resource, r.sell_price, r.buy_price
     FROM status AS s, market AS r, current_tick AS t;'
);
```

## Using Edbit

When playing, one typically needs a lot of information to make good decisions. It can be tedious to write
the same queries over and over, enev though one typically only need to press Ctrl-P to browse previously
written queries. Edbit supports monitored queries that windows containing a query and its answer, and
which is re-executed every time a query/command is executed in the main window. One can then
make many such windows, and arrange them accross your screen. This lets you have several
continously updated (i.e.~on every command you execute) queries, always visible. To create such a monitored
window, simply write a query (in the main Edbit window), execute it, and press the `M`-key.
Typically, one would want at least the following two queries open in separate monitored windows:

```sql
SELECT * FROM status;
SELECT * FROM current_system;
```

Below is a screenshot of how this would look with the two queries above together
with a query finding the 10 closest systems as monitored windows to the right of the screen,
with the main window on the left:

![Monitored windows in Edbit](./monitored_windows.png)

For more details on how to use Edbit, see the README-file in [Edbit's repo](https://gitlab.com/leifhka/edbit).

## Starting out

This section outlines a suggested way to start playing for the first time.
Start by executing the following commands, that will set up some useful
tick-queries and `VIEW`s:

```sql
-- Store all seen markets using tick-query:
CREATE TABLE historical_markets(
  tick int, pid int, planet text, system text, -- context with planet's id
  rid int, resource text, sell_price float, buy_price float -- market info
);
SELECT insert_tick_command(
    'markets',
    'INSERT INTO historical_markets
     SELECT t.tick, p.id, s.cur_planet, s.cur_system,
            r.id, r.resource, r.sell_price, r.buy_price
     FROM status AS s JOIN planets AS p ON (s.cur_planet = p.name),
          market AS r, current_tick AS t;'
);

-- Set up a VIEW that shows best buy-sell route in current solar system:
CREATE VIEW best_route AS
SELECT b.tick AS buy_tick, b.pid AS buy_from, s.tick AS sell_tick,
       s.pid AS sell_to, rid, resource, s.sell_price - b.buy_price AS profit
FROM historical_markets AS b JOIN historical_markets AS s USING (rid, resource)
ORDER BY profit DESC;

-- Make a round-trip to all planets in your
-- current solar system:
SELECT fly(id), tick() -- fly to planet nr. id then end turn
FROM planets;
```

(Note that the above commands can be put into a script for easy execution
in other games if you start over.)

You can now make trade rounds with the following simple query:

```sql
SELECT fly(buy_from), buy(rid, load_max), tick(), fly(sell_to), sell_all()
FROM best_route, status
ORDER BY profit DESC
LIMIT 1;
```

If you are using Edbit, now is also a good time to create several monitored
windows keeping track of you status, earnings, and how profitable your
trade-routes actually are.

Note that as time goes by (tick by tick) your information will be outdated,
i.e. the prices stored in `historical_markets` are no longer representative,
as you and other actors have bought and sold resources and the planets have
consumed resources, all affecting the prices. You can make similar round-trips
as above to collect new information from all planets, however
you should alter the `best_route` `VIEW` to only use the newest information.

Once you have made a decent amount of money, you can think about upgrading your ship,
try out mining, or warping to other systems. Note that if you warp to a different system,
the `best_route`-view no longer works, as it does not contain information about
which planet is in which system, and does not limit the results to
the system you are in. Try to fix these issues, and in general automate as much as possible,
create tables and tick-commands to gather all relevant information,
and make a query that always performs the best possible action given the information you
have.

You can always see you standing in the `money_rating`-column in the `status`-view.

Good luck, good learning, and have fun! :)
