DROP SCHEMA IF EXISTS dss CASCADE;

BEGIN;

\echo 'Setting up schema...'

CREATE SCHEMA dss;

--[[NOT GENERATED]]--

-- Resource categories
CREATE TABLE dss.category (
    name text PRIMARY KEY
);

INSERT INTO dss.category VALUES
('Ore'), ('Food'), ('Liquid'), ('Luxury'), ('Biological');

CREATE TABLE dss.script (
  name text PRIMARY KEY,
  code text NOT NULL
);

CREATE TABLE dss.faction (
    fid int UNIQUE NOT NULL,
    name text PRIMARY KEY,
    upgrade text NOT NULL,
    description text NOT NULL
);

INSERT INTO dss.faction VALUES
(1, 'sznerckk', 'mining efficiency', 'A bug-like faction with exceptional mining-efficiency.'),
(2, 'ooulium', 'warp_efficiency', 'A peacefull faction with highly advanced warp-drives.'),
(3, 'carbonae', 'fly_efficiency', 'A human-like faction with highly developed rocket engines.'),
(4, 'sonos', 'load_max', 'A robot faction with very large ships.');

CREATE TABLE dss.constants(
  name text PRIMARY KEY,
  value text NOT NULL
);

INSERT INTO dss.constants VALUES
('vat', '1.25'), -- value added tax
('upgrade_price_mod', '50.0'), -- upgrade price multiplier
('warp_price_mod', '0.05'), -- fuel price multiplier for warping
('fly_price_mod', '5'), -- fuel price multiplier for flying
('warp_price', '100'), -- max fuel price for warping (used by NPCs as aprox)
('fly_price', '10'); -- max fuel price for flying (used by NPCs as aprox)

CREATE OR REPLACE FUNCTION constant(n text) RETURNS text AS
$body$
  SELECT value FROM dss.constants WHERE name = n;
$body$ language sql;

CREATE TABLE dss.planet_symbol(nr serial PRIMARY KEY, symbol text UNIQUE NOT NULL);
INSERT INTO dss.planet_symbol(symbol) VALUES
('.'), (','), ('o'), ('O'), ('0'),
('ø'), ('Ø'), ('¤'), ('*'), ('Q');

--[[GENERATED]]--

CREATE TABLE dss.resource (
  id SERIAL UNIQUE NOT NULL,
  name text PRIMARY KEY,
  faction text NOT NULL,
  category text REFERENCES dss.category(name),
  value float NOT NULL CHECK (value > 0)
);

CREATE INDEX ON dss.resource(id);
CREATE INDEX ON dss.resource(value);

CREATE TABLE dss.system (
  name text PRIMARY KEY,
  faction text NOT NULL,
  pos float[] NOT NULL 
);

-- Each planet also has a mine which one can
-- mine a particular resource from
CREATE TABLE dss.planet (
  id int NOT NULL,
  name text,
  system text REFERENCES dss.system(name),
  symbol text REFERENCES dss.planet_symbol(symbol),
  mine_type text REFERENCES dss.resource(name),
  mine_stock int NOT NULL CHECK (mine_stock >= 0),
  provides_upgrades boolean NOT NULL,
  pos float[] NOT NULL,
  receptionist_name text NOT NULL,
  receptionist_portrait text NOT NULL,
  PRIMARY KEY (name, system),
  UNIQUE (id, system)
);

CREATE INDEX ON dss.planet(id);
CREATE INDEX ON dss.planet(name);
CREATE INDEX ON dss.planet(system);
CREATE INDEX ON dss.planet(mine_type);

-- Number of a given resource a particular system needs/wants
-- The price the planet is willing to pay is derived
-- from this value (see resource_price below)
CREATE TABLE dss.resource_stock (
  planet text NOT NULL,
  system text NOT NULL,
  resource text REFERENCES dss.resource(name),
  stock int NOT NULL CHECK (stock >= 0),
  use int NOT NULL CHECK (use >= 0),
  FOREIGN KEY (planet, system) REFERENCES dss.planet(name, system),
  PRIMARY KEY (planet, system, resource)
);

CREATE INDEX ON dss.resource_stock(planet);
CREATE INDEX ON dss.resource_stock(system);
CREATE INDEX ON dss.resource_stock(resource);

CREATE VIEW dss.resource_price AS
SELECT 
  id,
  planet,
  system,
  resource,
  stock,
  sqrt(10*use::float / greatest(stock, 1) )*value AS sell_price,
  sqrt(greatest(10*use, 1)::float  / greatest(stock, 1))*value*constant('vat')::float AS buy_price
FROM dss.resource AS r
     JOIN dss.resource_stock AS rs ON (rs.resource = r.name);

CREATE TABLE dss.actor (
  aid serial PRIMARY KEY,
  name text NOT NULL UNIQUE,
  faction text NOT NULL CHECK (faction IN ('sznerckk', 'ooulium', 'sonos', 'carbonae')),
  is_player boolean NOT NULL, -- True for actual player (false for NPCs)
  money float NOT NULL CHECK (money >= 0),
  cur_planet text NOT NULL,
  cur_system text NOT NULL,
  load int CHECK (load >= 0 AND load <= load_max),
  load_type text REFERENCES dss.resource(name),
  load_max int NOT NULL CHECK (load_max >= 0),
  fly_efficiency int NOT NULL CHECK (fly_efficiency >= 0),
  warp_efficiency int NOT NULL CHECK (warp_efficiency >= 0),
  mining_efficiency int NOT NULL CHECK (mining_efficiency >= 0),
  moved boolean NOT NULL,
  acted boolean NOT NULL,
  script text REFERENCES dss.script(name),
  state text, -- Used by script for keeping state
  FOREIGN KEY (cur_planet, cur_system) REFERENCES dss.planet(name, system)
);

CREATE INDEX ON dss.actor(cur_system, cur_planet);
CREATE INDEX ON dss.actor(name);
CREATE INDEX ON dss.actor(is_player) WHERE is_player;

CREATE VIEW dss.actor_context AS
SELECT aid, a.name, a.faction, a.is_player, a.money, 
       a.cur_planet, a.cur_system, a.load, a.load_type, a.load_max,
       a.fly_efficiency, a.warp_efficiency, a.mining_efficiency,
       a.moved, a.acted, a.script, a.state,
       p.mine_type, p.mine_stock,
       sy.faction AS system_faction
FROM dss.actor AS a
     JOIN dss.planet AS p ON (a.cur_planet = p.name AND a.cur_system = p.system)
     JOIN dss.system AS sy ON (a.cur_system = sy.name);

-- Scripts

CREATE OR REPLACE FUNCTION dss.set_state(actor_id int, new_state text) RETURNS void AS
$body$
  UPDATE dss.actor
  SET state = new_state
  WHERE aid = actor_id;
$body$ language sql;

INSERT INTO dss.script VALUES
('miner',
$script$
  SELECT 
    CASE
      WHEN c.state = 'mine' AND c.load < c.load_max THEN
        'SELECT dss.mine(%1$s), dss.stay(%1$s);'
      WHEN money < 1 OR
           NOT EXISTS (SELECT 1 FROM dss.planet
                       WHERE mine_stock > 0 AND system = c.cur_system) THEN
        'SELECT dss.stay(%1$s), dss.wait(%1$s);'
      WHEN c.state = 'mine' AND c.load = c.load_max THEN
        'SELECT dss.fly(%1$s, ''' ||
                    (SELECT planet FROM dss.resource_price
                     WHERE system = c.cur_system AND resource = c.load_type
                     ORDER BY sell_price DESC LIMIT 1) || '''),
                dss.sell_all(%1$s),
                dss.set_state(%1$s, ''fly_mine'');'
      WHEN c.state = 'fly_mine' THEN
        'SELECT dss.fly(%1$s, ''' ||
                    (SELECT p.name
                     FROM dss.planet AS p JOIN dss.resource_price AS r
                          ON (p.name = r.planet AND p.system = r.system)
                     WHERE p.system = c.cur_system AND p.mine_stock > 0 AND
                           p.mine_type = r.resource
                     ORDER BY round(log(greatest(1, r.sell_price))) DESC, random()
                     LIMIT 1) || '''),
                dss.set_state(%1$s, ''mine''),
                dss.mine(%1$s);' 
      ELSE 'SELECT dss.set_state(%1$s, ''fly_mine'');'
    END
  FROM dss.actor_context AS c WHERE aid = %1$s;
$script$),
('trader',
$script$
  SELECT 
    CASE
      WHEN c.money < constant('fly_price')::float OR
           NOT EXISTS (SELECT 1
                       FROM dss.resource_price AS p1
                            JOIN dss.resource_price AS p2 USING (resource, system)
                       WHERE  p1.sell_price > p2.buy_price AND system = c.cur_system) THEN
        'SELECT dss.stay(%1$s), dss.wait(%1$s);'
      WHEN c.state = 'sell' THEN
        'SELECT dss.fly(%1$s, ''' ||
                    (SELECT planet FROM dss.resource_price
                     WHERE system = c.cur_system AND resource = c.load_type
                     ORDER BY sell_price DESC LIMIT 1) || '''),
                dss.sell_all(%1$s),
                dss.set_state(%1$s, ''buy'');'
      WHEN c.state = 'buy' THEN
        'SELECT dss.fly(%1$s, b.planet),
                dss.buy(%1$s, resource, least(b.stock,
                                          floor(greatest(0, (' || c.money || ' - constant(''fly_price'')::float))/b.buy_price),
                                          ' || c.load_max || ')::int),
                dss.set_state(%1$s, ''sell'')
         FROM dss.resource_price AS b
              JOIN dss.resource_price AS s USING (system, resource)
         WHERE system = ''' || c.cur_system || '''
         ORDER BY round((s.sell_price - b.buy_price)) DESC, random()
         LIMIT 1;'
      ELSE 'SELECT dss.set_state(%1$s, ''buy'');'
    END
  FROM dss.actor_context AS c WHERE aid = %1$s;
$script$),
('warp_trader',
$script$
  SELECT 
    CASE
      WHEN c.state != 'buy' AND c.money < constant('warp_price')::float THEN
        'SELECT dss.stay(%1$s), dss.wait(%1$s);'
      WHEN c.state = 'buy' THEN
        'SELECT dss.fly(%1$s, b.planet),
                dss.buy(%1$s, b.resource,
                        least(b.stock,
                              floor(greatest(0, (' || c.money || ' - 3*constant(''warp_price'')::float))/b.buy_price),
                              ' || c.load_max || ')::int
                       ),
                dss.set_state(%1$s, b.buy_price::text)
         FROM dss.resource_price AS b
         WHERE b.system = ''' || c.cur_system || '''
         ORDER BY b.buy_price, random()
         LIMIT 1;'
      WHEN c.state != 'buy' AND 
           NOT EXISTS (SELECT 1
                       FROM dss.resource_price
                       WHERE resource = c.load_type AND
                             sell_price > 100*c.state::float AND
                             system = c.cur_system) THEN
        'SELECT dss.warp(%1$s, ''' ||
                         (SELECT name FROM dss.system
                          WHERE name != c.cur_system ORDER BY random() LIMIT 1) || '''
                        );'
      WHEN c.state != 'buy' THEN
        'SELECT dss.fly(%1$s, ''' ||
                        (SELECT planet FROM dss.resource_price
                         WHERE system = c.cur_system AND resource = c.load_type
                         ORDER BY sell_price DESC LIMIT 1) || '''
                       ),
                dss.sell_all(%1$s),
                dss.set_state(%1$s, ''buy'');'
      ELSE 'SELECT dss.set_state(%1$s, ''buy'');'
    END
  FROM dss.actor_context AS c WHERE aid = %1$s;
$script$);

CREATE SEQUENCE dss.turn;
SELECT nextval('dss.turn'); -- Init turn to 1

-- User tick-queries

CREATE TABLE dss.tick_commands(name text PRIMARY KEY, command text);

CREATE OR REPLACE FUNCTION dss.exec_tick_command(command text) RETURNS void AS
$body$
DECLARE
  ins text;
  trn int;
BEGIN
  EXECUTE command;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.exec_tick_commands() RETURNS void AS
$body$
BEGIN
  PERFORM dss.exec_tick_command(command)
  FROM dss.tick_commands;
END;
$body$ language plpgsql;

-- Player schema
CREATE OR REPLACE FUNCTION dss.distance(pos1 float[], pos2 float[]) RETURNS float AS
$body$
  SELECT sqrt(pow( (pos1[1] - pos2[1]) , 2) + pow( (pos1[2] - pos2[2]) , 2));
$body$ language sql;

CREATE OR REPLACE FUNCTION dss.warp_price(fsystem text, tsystem text, warp_efficiency int, load int) RETURNS float AS
$body$
  SELECT (constant('warp_price_mod')::float * sqrt(1.0 + (load/100.0)) * dss.distance(fsy.pos, tsy.pos))/(1 + (warp_efficiency/10.0))
  FROM dss.system AS fsy, dss.system AS tsy
  WHERE fsy.name = fsystem AND tsy.name = tsystem;
$body$ language sql;

CREATE OR REPLACE FUNCTION dss.fly_price(csystem text, fplanet text, tplanet text, fly_efficiency int, load int)
RETURNS float AS
$body$
  SELECT constant('fly_price_mod')::float * sqrt(1.0 + (load/100.0)) * dss.distance(fpl.pos, tpl.pos)/(1 + (fly_efficiency/10.0))
  FROM dss.planet AS fpl JOIN dss.planet AS tpl USING (system)
  WHERE system = csystem AND fpl.name = fplanet AND tpl.name = tplanet;
$body$ language sql;

CREATE OR REPLACE FUNCTION dss.upgrade_price(usystem text, uplanet text)
RETURNS float AS
$body$
  SELECT constant('upgrade_price_mod')::float*(1 + avg(r.buy_price))
  FROM dss.planet AS p JOIN dss.resource_price AS r ON (p.system = r.system AND p.name = r.planet)
  WHERE p.system = usystem AND p.name = uplanet;
$body$ language sql;


CREATE VIEW systems AS
SELECT t.name, t.faction, t.pos,
       dss.distance(t.pos, f.pos) AS distance,
       dss.warp_price(f.name, t.name, a.warp_efficiency, a.load) AS warp_price
FROM dss.system AS t, dss.system AS f, dss.actor AS a
WHERE f.name = a.cur_system AND a.is_player;

CREATE VIEW system AS
SELECT s.name, s.faction,
  (SELECT count(*) FROM dss.planet AS p WHERE p.system = s.name) AS num_planets,
  (SELECT count(*) FROM dss.actor AS a WHERE a.cur_system = s.name) AS num_actors
FROM dss.system AS s
WHERE name = (SELECT cur_system FROM dss.actor WHERE is_player);

CREATE VIEW planets AS
SELECT t.id, t.name, t.symbol,
       t.pos,
       dss.distance(t.pos, f.pos) AS distance,
       dss.fly_price(system, f.name, t.name, a.fly_efficiency, a.load) AS fly_price
FROM dss.planet AS t JOIN dss.planet AS f USING (system), dss.actor AS a
WHERE system = a.cur_system AND f.name = a.cur_planet AND a.is_player;

CREATE VIEW planet AS
SELECT p.id,
  p.name,
  s.faction,
  p.mine_type, 
  CASE WHEN p.mine_stock > 100 THEN 'rich'
       WHEN p.mine_stock = 0 THEN 'empty'
       ELSE 'poor'
  END AS mine_stock,
  CASE WHEN p.provides_upgrades THEN f.upgrade ELSE NULL END AS provides_upgrades,
  CASE WHEN p.provides_upgrades THEN dss.upgrade_price(p.system, p.name) ELSE NULL END AS upgrade_price,
  p.receptionist_name,
  p.receptionist_portrait
FROM dss.planet AS p
     JOIN dss.system AS s ON (p.system = s.name)
     JOIN dss.actor AS a ON (a.cur_planet = p.name AND a.cur_system = p.system)
     JOIN dss.faction AS f ON (s.faction = f.name)
WHERE a.is_player;

CREATE VIEW market AS
SELECT p.id,
  p.resource,
  p.sell_price,
  p.buy_price
FROM dss.resource_price AS p
     JOIN dss.system AS s ON (p.system = s.name)
     JOIN dss.actor AS a ON (a.cur_planet = p.planet AND a.cur_system = p.system)
WHERE a.is_player;

CREATE VIEW actors AS
SELECT o.name, o.faction, o.script AS occupation
FROM dss.actor AS o 
     JOIN dss.actor AS a USING (cur_planet, cur_system)
WHERE a.is_player;

CREATE VIEW status AS
SELECT name, faction, money,
  cur_planet, cur_system,
  load, load_type, load_max,
  warp_efficiency, fly_efficiency, mining_efficiency,
  moved, acted,
  (SELECT p.money/avg(o.money) FROM dss.actor o) AS money_rating
FROM dss.actor AS p
WHERE is_player;

CREATE VIEW tick_commands AS
SELECT * FROM dss.tick_commands;

-- View for drawing current solar system
CREATE VIEW current_system(current_system) AS
WITH
  bounds AS (
    SELECT max(pos[1]) AS max_x,
           min(pos[1]) AS min_x,
           max(pos[2]) AS max_y,
           min(pos[2]) AS min_y
      FROM planets
  ),
  steps AS (
    SELECT (max_x - min_x)/30 AS sx,
           (max_y - min_y)/15 AS sy
      FROM bounds
  ),
  points AS (
    SELECT dx, dy, name, pos, symbol
      FROM planets AS p,
           generate_series(0, 30) AS v1(dx),
           generate_series(0, 15) AS v2(dy),
           bounds,
           steps
     WHERE abs(pos[1] - (min_x + dx*sx)) <= sx/2.0 AND
           abs(pos[2] - (min_y + dy*sy)) <= sy/2.0
  ),
  lines AS (
    SELECT y, x,
           CASE -- TODO: Ensure no collisions 
             WHEN 1 = (SELECT count(*) FROM points WHERE dx = x AND dy = y)
                 THEN (SELECT symbol FROM points WHERE dx = x AND dy = y)
             WHEN 2 <= (SELECT count(*) FROM points WHERE dx = x AND dy = y)
                 THEN '8'
             WHEN x = 15 AND y = 7
                 THEN '@'
             ELSE ' '
           END AS sign
      FROM generate_series(0, 30) AS t1(x),
           generate_series(0, 15) AS t2(y)
  ),
  map AS (
      SELECT y+1 AS n, -- start counting at 1
             string_agg(sign, '' ORDER BY x) AS line
        FROM lines
    GROUP BY y
  ),
  nplanets AS (
     SELECT row_number() OVER () AS n, *
       FROM planets
  )
SELECT line, id, symbol, name, distance, fly_price
  FROM map LEFT JOIN nplanets USING (n);

CREATE VIEW current_tick(tick) AS
SELECT last_value FROM dss.turn;

CREATE TABLE game_functions(name text PRIMARY KEY, ftype text, description text);
INSERT INTO game_functions VALUES
('constant(constant_name text)', 'Info', 'Returns the value of the system constant, such as ''vat'', ''warp_price_mode'' and ''fly_price_mod'''),
('player_aid()', 'Info', 'Returns the aid-value of the current player.'),
('tick()', null, 'Ends the turn. After tick is called, tick-queries are executed, all NPCs can act and move, and planets consume resources.'),
('warp(to_system text)', 'Move', 'Warps the player to the argument system. Gives error if not enough money for warp or system does not exist.'),
('fly(to_planet int)', 'Move', 'Flies the player to the planet given by the argument id within the current system. Gives error if not enough money for flight or planet does not exist (in current system).'),
('mine()', 'Act', 'Mines the current planet for resources.'),
('sell(num int)', 'Act', 'Sells the argument number of resoures currently loaded on ship.'),
('sell_all()', 'Act', 'Sells the all of the resoures currently loaded on ship.'),
('buy(resource int, num int)', 'Act', 'Buys the argument number of the resoure with the argument id from market on current planet into ship''s load. Gives error if product does not exists, if resource to buy is not same as currently loaded resource (if any) or if the number of resources after buy exceeds capacity (max_load) of ship.'),
('upgrade(num int)', 'Act', 'Buys the argument number of the upgrades provided by the current planet to player''s ship. Gives error if planet does not provide upgrades or if not enough money to buy the given number of upgrades.'),
('insert_tick_command(name text, command text)', null, 'Creates a tick-command with name equal to first argument, that on each end of turn executes the argument commadn. The command can be any of INSERT, UPDATE, DELETE, TRUNCATE, etc.'), 
('delete_tick_command(name text)', null, 'Deletes the tick-query with the given argument as name.');

CREATE TABLE game_tables(name text PRIMARY KEY, description text);
INSERT INTO game_tables VALUES
('systems', 'Describes all systems in the universe.'),
('planets', 'Describes all planets in the current system (i.e. the system the player is currently in).'),
('planet', 'Describes the current planet (i.e. the planet the player is currently on) with potentil upgrades the player can buy for its ship.'),
('market', 'Lists all resources with buy and sell prices for the current planet.'),
('actors', 'Lists all actors (NPCs) currently on the current planet.'),
('status', 'Describes the status of the player (position, money, load, score, etc.).'),
('current_tick', 'Contains the integer value that denotes the current tick/turn.'),
('tick_commands', 'Lists all active tick-commands made by the player.'),
('current_system', 'Similar to the table planets, but also gives an ASCII-map of the current system.'),
('game_functions', 'Describes all functions the player can use to act and move in the game.'),
('game_tables', 'This table.');

COMMIT;

