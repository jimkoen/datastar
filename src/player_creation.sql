BEGIN;

\echo 'Inserting new player into universe...'
SELECT current_user AS new_player;

SELECT fid, name, description FROM dss.faction;
\prompt 'Please select a faction (fid): ' player_faction
CALL dss.create_player(:player_faction);

--REVOKE ALL ON SCHEMA dss FROM current_user;
--GRANT ALL ON ALL TABLES IN SCHEMA public TO current_user;

COMMIT;
