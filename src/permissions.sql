\set player 'player'
\set password 'player'
\set database 'datastar'\\

BEGIN;

-- Create Postgis used for planet and system locations
CREATE EXTENSION IF NOT EXISTS postgis;

-- Create user for player
CREATE USER :player PASSWORD :'password';
GRANT ALL ON DATABASE :database TO :player;
GRANT ALL ON SCHEMA public TO :player;
GRANT ALL ON ALL TABLES IN SCHEMA public TO :player;

-- Disable grants
CREATE FUNCTION raise_grant_exception()
RETURNS event_trigger AS
$body$
BEGIN
    RAISE EXCEPTION 'GRANT not allowed in this database and is disabled!';
END;
$body$ LANGUAGE plpgsql;

CREATE EVENT TRIGGER disable_grant_trigger
ON ddl_command_end
WHEN TAG IN ('GRANT')
EXECUTE FUNCTION raise_grant_exception();

COMMIT;

-- Disable grants
-- CREATE FUNCTION raise_grant_exception()
-- RETURNS event_trigger AS
-- $body$
-- DECLARE
--     obj record;
-- BEGIN
--     FOR obj IN SELECT * FROM pg_event_trigger_ddl_commands()
--     LOOP
--         IF obj.schema_name = 'dss' THEN
--           RAISE EXCEPTION 'GRANT not allowed in this database and is disabled!';
--         END IF;
--     END LOOP;
-- END;
-- $body$ LANGUAGE plpgsql;
