-- Functions and triggers for changes to be applied at each tick
-- E.g. planets use resource, ships move, ships mine, etc.

BEGIN;

CREATE OR REPLACE PROCEDURE dss.create_player(player_fid int) AS
$body$
DECLARE
    faction_name text;
BEGIN
    faction_name := (SELECT name FROM dss.faction WHERE fid = player_fid);

    ASSERT (faction_name IS NOT NULL),
        'Number is not the fid of a faction!';

    INSERT INTO dss.actor(name, faction, is_player, money, cur_planet, cur_system,
                          load, load_type, load_max, warp_efficiency, fly_efficiency, mining_efficiency,
                          moved, acted, script, state)
    WITH
      loc AS (
        SELECT p.name, p.system
        FROM dss.planet AS p JOIN dss.system AS s ON (p.system = s.name)
        WHERE s.faction = faction_name
        ORDER BY random() LIMIT 1
      )
    SELECT 
      current_user,
      faction_name,
      true,
      300,
      name AS cur_planet,
      system AS cur_system,
      0 AS load,
      NULL as load_type,
      CASE WHEN faction_name = 'sonos' THEN 20 ELSE 10 END AS load_max,
      CASE WHEN faction_name = 'ooulium' THEN 5 ELSE 0 END AS warp_efficiency,
      CASE WHEN faction_name = 'carbonae' THEN 5 ELSE 0 END AS fly_efficiency,
      CASE WHEN faction_name = 'sznerckk' THEN 5 ELSE 0 END AS mining_efficiency,
      false,
      false,
      NULL,
      NULL
    FROM loc;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.acted(actor_id int) RETURNS void AS
$body$
BEGIN
  ASSERT -- Check not acted
    (SELECT NOT acted FROM dss.actor WHERE aid = actor_id),
    'Already acted this tick!';

  UPDATE dss.actor
  SET acted = true
  WHERE aid = actor_id;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.moved(actor_id int) RETURNS void AS
$body$
BEGIN
  ASSERT -- Check not moved
    (SELECT NOT moved FROM dss.actor WHERE aid = actor_id),
    'Already moved this tick!';

  UPDATE dss.actor
  SET moved = true
  WHERE aid = actor_id;
END;
$body$ language plpgsql;

CREATE TYPE dss.warp_info AS (
  price float,
  to_planet text
);

CREATE OR REPLACE FUNCTION dss.warp(actor_id int, csystem text) RETURNS dss.warp_info AS
$body$
DECLARE
  wprice float;
  a dss.actor;
BEGIN
  ASSERT -- Check that system exists
    (SELECT true FROM dss.system
     WHERE name = csystem),
    'System does not exist!';

  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  wprice := dss.warp_price(a.cur_system, csystem, a.warp_efficiency, a.load);

  ASSERT -- Check enough money for warping
    (wprice <= a.money),
    'Not sufficient money for warping!';

  UPDATE dss.actor
  SET cur_planet = (SELECT name FROM dss.planet AS p -- pick random planet in system
                    WHERE p.system = csystem
                    ORDER BY random() LIMIT 1),
      cur_system = csystem,
      money = money - wprice
  WHERE aid = actor_id;

  PERFORM dss.moved(actor_id);
  PERFORM dss.acted(actor_id);

  RETURN ROW(wprice, (SELECT cur_planet FROM dss.actor WHERE aid = actor_id));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.fly(actor_id int, cplanet text) RETURNS float AS
$body$
DECLARE
  fprice float;
  a dss.actor;
BEGIN
  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);

  ASSERT -- Check that planet is in current system
    (SELECT true FROM dss.planet
     WHERE name = cplanet AND
           system = a.cur_system),
    'Planet not in current system!';

  fprice := dss.fly_price(a.cur_system, a.cur_planet, cplanet, a.fly_efficiency, a.load);

  ASSERT -- Check enough money for flying
    (fprice <= a.money),
    'Not sufficient money for flying!';

  UPDATE dss.actor
  SET cur_planet = cplanet,
      money = money - fprice
  WHERE aid = actor_id;

  PERFORM dss.moved(actor_id);

  RETURN fprice;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.mine(actor_id int) RETURNS int AS
$body$
DECLARE
  to_mine int;
  a dss.actor;
BEGIN
  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);

  ASSERT -- Check enough space for minig
    (a.load_max > a.load),
    'Not sufficient space on ship for mining!';

  ASSERT -- Check correct current load type for minig
    (a.load_type IS NULL OR
     a.load_type = (SELECT mine_type FROM dss.planet
                    WHERE name = a.cur_planet AND system = a.cur_system)),
    'Can only have one type of load!';

  to_mine := (
    SELECT least(greatest(0, a.load_max - a.load),
                 mine_stock,
                 floor(2 + random()*6*(1 + a.mining_efficiency/10.0)))
    FROM dss.planet
    WHERE a.cur_planet = name AND a.cur_system = system
  );

  UPDATE dss.planet
  SET mine_stock = mine_stock - to_mine
  WHERE name = a.cur_planet AND system = a.cur_system;

  UPDATE dss.actor
  SET load = load + to_mine,
      load_type = (SELECT mine_type
                   FROM dss.planet AS p
                   WHERE p.name = cur_planet AND p.system = cur_system)
  WHERE aid = actor_id;

  PERFORM dss.acted(actor_id);

  RETURN to_mine;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.sell_all(actor_id int) RETURNS float AS
$body$
BEGIN
  RETURN dss.sell(actor_id, (SELECT load FROM dss.actor WHERE aid = actor_id));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.sell(actor_id int, n int) RETURNS float AS
$body$
DECLARE
  price float;
  a dss.actor;
BEGIN
  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  ASSERT -- Check that has load_type
    (a.load_type IS NOT NULL),
    'Cannot sell when not carrying anything';

  ASSERT -- Check that n <= load
    (n <= a.load),
    'Cannot sell more than one owns!';

  SELECT sell_price INTO price
  FROM dss.resource_price
  WHERE planet = a.cur_planet AND
        system = a.cur_system AND
        resource = a.load_type;

  UPDATE dss.actor
  SET load = load - n,
      load_type = CASE WHEN load = n THEN NULL ELSE load_type END,
      money = money + n*price
  WHERE aid = actor_id;

  UPDATE dss.resource_stock
  SET stock = stock + n
  WHERE planet = a.cur_planet AND system = a.cur_system;

  PERFORM dss.acted(actor_id);
  RETURN price*n;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.buy(actor_id int, res text, n int) RETURNS float AS
$body$
DECLARE
  a dss.actor;
  price float;
  total_price float;
BEGIN
  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  price := (SELECT buy_price FROM dss.resource_price
            WHERE resource = res AND
                  planet = a.cur_planet AND
                  system = a.cur_system);
  total_price := n * price;
  
  ASSERT -- Check that product exists
    (price IS NOT NULL),
    format('Resource %s does not exist!', res);

  ASSERT -- Check that has no load_type or same as product to buy
    (a.load_type IS NULL OR a.load_type = res),
    'Can only load one type of resource! (Load type is not same as bought product)';

  ASSERT -- Check that load_max - load >= n
    (n <= a.load_max - a.load),
    format('Not room for loading %s resources on ship!', n);

  ASSERT
    (a.money >= total_price),
    format('Not enough money to buy %s of %s.', n, res);

  UPDATE dss.actor
  SET money = money - total_price
  WHERE aid = actor_id;

  UPDATE dss.resource_stock
  SET stock = stock - n
  WHERE planet = a.cur_planet AND
        system = a.cur_system AND
        resource = a.load_type;

  UPDATE dss.actor
  SET load = load + n,
      load_type = res
  WHERE aid = actor_id;

  PERFORM dss.acted(actor_id);
  RETURN total_price;
END;
$body$ language plpgsql;


CREATE OR REPLACE FUNCTION dss.consume() RETURNS void AS
$body$
  UPDATE dss.resource_stock
  SET stock = greatest(stock - use, 0);
$body$ language sql;

CREATE OR REPLACE FUNCTION dss.wait(actor_id int) RETURNS void AS
$body$
BEGIN
  PERFORM dss.acted(actor_id);
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.stay(actor_id int) RETURNS void AS
$body$
BEGIN
  PERFORM dss.moved(actor_id);
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.act_for(actor_id int) RETURNS void AS
$body$
DECLARE
  acode text;
  scode text;
  command text;
BEGIN
  SELECT code INTO acode -- find actor's code to execute
  FROM dss.script
  WHERE name = (SELECT script FROM dss.actor WHERE aid = actor_id);

  scode := format(acode, actor_id); -- subsitute in actor_id for placeholder
  EXECUTE scode INTO command; -- execute script to obtain command to execute
  EXECUTE command; 
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.act() RETURNS void AS
$body$
BEGIN
  PERFORM dss.act_for(aid)
  FROM dss.actor
  WHERE not is_player
  ORDER BY aid;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION tick() RETURNS int AS
$body$
BEGIN
  PERFORM dss.exec_tick_commands();

  UPDATE dss.actor
  SET moved = false,
      acted = false;

  PERFORM dss.consume();
  PERFORM dss.act();

  RETURN (SELECT nextval('dss.turn') AS turn);
END;
$body$ language plpgsql;


CREATE OR REPLACE FUNCTION dss.simulate_random_once(num_actors bigint) RETURNS void AS
$body$
BEGIN
  UPDATE dss.actor
  SET moved = false,
      acted = false;

  PERFORM dss.act_for(aid)
  FROM dss.actor
  ORDER BY random()
  LIMIT num_actors;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.simulate_random(num_actors bigint, turns int) RETURNS bigint AS
$body$
BEGIN
  PERFORM dss.simulate_random_once(num_actors)
  FROM generate_series(1, turns) AS t;

  RETURN num_actors * turns;
END;
$body$ language plpgsql;

--[[Player functions]]

-- Actions

CREATE OR REPLACE FUNCTION player_aid() RETURNS int AS
$body$
  SELECT aid FROM dss.actor WHERE is_player;
$body$ language sql;

CREATE OR REPLACE FUNCTION greet_warp(to_system text, to_planet text) RETURNS text AS
$body$
  SELECT p.receptionist_portrait
         || E'\n\nPlanet receptionist ' || p.receptionist_name || E':\n'
         || '"Welcome warper to the ' || s.faction || E'ian planet\n'
         || 'of ' || p.name || ' in the ' || s.name || ' system.'
         || CASE WHEN p.provides_upgrades
                 THEN E'\nThis planet provides upgrades for your\nship''s ' || f.upgrade || '!"'
                 ELSE '"'
            END
  FROM dss.planet AS p
       JOIN dss.system AS s ON (p.system = s.name)
       JOIN dss.faction AS f ON (s.faction = f.name)
  WHERE s.name = to_system AND p.name = to_planet;
$body$ language sql;

CREATE OR REPLACE FUNCTION greet_fly(to_system text, to_planet text) RETURNS text AS
$body$
  SELECT p.receptionist_portrait
         || E'\n\nPlanet receptionist ' || p.receptionist_name || E':\n'
         || '"Welcome traveler to the planet ' || p.name || '.'
         || CASE WHEN p.provides_upgrades
                 THEN E'\nThis planet provides upgrades for your\nship''s ' || f.upgrade || '!"'
                 ELSE '"'
            END
  FROM dss.planet AS p
       JOIN dss.system AS s ON (p.system = s.name)
       JOIN dss.faction AS f ON (s.faction = f.name)
  WHERE p.system = to_system AND p.name = to_planet;
$body$ language sql;

CREATE OR REPLACE FUNCTION warp(to_system text) RETURNS text AS
$body$
DECLARE
  actor_id int;
  wi dss.warp_info;
BEGIN
  wi := dss.warp(player_aid(), to_system);
  RETURN format('*Warped to system %s and landed on planet %s, paid %s in fuel.*', to_system, wi.to_planet, round(wi.price::numeric, 2))
    || E'\n\n' || greet_warp(to_system, wi.to_planet);
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION fly(to_planet text) RETURNS text AS
$body$
DECLARE
  actor_id int;
  pname text;
  fprice float;
BEGIN
  SELECT aid, t.name INTO actor_id, pname
  FROM dss.actor AS a JOIN dss.planet AS t ON (a.cur_system = t.system)
  WHERE a.is_player AND (t.id::text = to_planet OR t.name = to_planet);

  fprice = dss.fly(actor_id, pname);
  RETURN format('*Landed on planet %s, paid %s in fuel.*', pname, round(fprice::numeric, 2))
    || E'\n\n'
    || greet_fly((SELECT cur_system FROM dss.actor WHERE aid = actor_id), pname);
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION fly(to_planet int) RETURNS text AS
$body$
  SELECT fly(to_planet::text);
$body$ language sql;

CREATE OR REPLACE FUNCTION mine() RETURNS text AS
$body$
DECLARE
  mined int;
BEGIN
  mined := dss.mine(player_aid());
  RETURN format('*Mined %s of %s.*', mined, (SELECT load_type FROM dss.actor WHERE is_player));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION sell(n int) RETURNS text AS
$body$
DECLARE
  lt text;
  total_price float;
BEGIN
  lt := (SELECT load_type FROM dss.actor WHERE is_player);
  total_price := dss.sell(player_aid(), n);
  RETURN format('*Sold %s %s(s) for %s.*', n, lt, round(total_price::numeric, 2));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION sell_all() RETURNS text AS
$body$
DECLARE
  lt text;
BEGIN
  RETURN sell((SELECT load FROM dss.actor WHERE is_player));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION buy(prod int, n int) RETURNS text AS
$body$
DECLARE
  prodname text;
  total_price float;
BEGIN
  SELECT name INTO prodname FROM dss.resource WHERE id = prod;
  total_price := dss.buy(player_aid(), prodname, n);
  RETURN format('*Bought %s %s(s) for %s.*', n, prodname, round(total_price::numeric, 2));
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION upgrade(n int) RETURNS text AS
$body$
DECLARE
  total_price float;
  upgrade_type text;
BEGIN
  SELECT n*upgrade_price, provides_upgrades INTO total_price, upgrade_type FROM planet;

  ASSERT -- Check that planet can upgrade
    (total_price IS NOT NULL),
    'Planet does not provide upgrades!';

  ASSERT -- Check enough money
    (total_price <= (SELECT money FROM dss.actor WHERE is_player)),
    'Not enough money for upgrade!';

  EXECUTE format('UPDATE dss.actor
                  SET money = money - %s,
                      %s = %s + %s
                  WHERE is_player;',
                 total_price, upgrade_type, upgrade_type, n);

  PERFORM dss.acted(player_aid());
  RETURN format('*Bought %s upgrades of %s.*', n, upgrade_type);
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION insert_tick_command(name text, command text)
RETURNS void AS
$body$
  INSERT INTO dss.tick_commands VALUES (name, command);
$body$ language sql;

CREATE OR REPLACE FUNCTION delete_tick_command(nm text)
RETURNS void AS
$body$
  DELETE FROM dss.tick_commands WHERE name = nm;
$body$ language sql;


COMMIT;
